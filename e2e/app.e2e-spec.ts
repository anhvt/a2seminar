import { A2seminarPage } from './app.po';

describe('a2seminar App', function() {
  let page: A2seminarPage;

  beforeEach(() => {
    page = new A2seminarPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
