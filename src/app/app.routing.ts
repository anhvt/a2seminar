import {Routes, RouterModule} from "@angular/router";
import {NgModule} from "@angular/core";
import {DatabindingComponent} from "./databinding/databinding.component";
import {DashboardComponent} from "./dashboard/dashboard.component";
import {FormComponent} from "./form/form.component";
import {HttpComponent} from "./http/http.component";
import {DirectiveComponent} from "./directive/directive.component";
import {PipeComponent} from "./pipe/pipe.component";
import {ServiceComponent} from "./service/service.component";

export const routes: Routes = [
  {path: '', redirectTo: 'dashboard', pathMatch: 'full',},
  {path: 'dashboard', component: DashboardComponent},
  {path: 'data-binding', component: DatabindingComponent},
  {path: 'directive', component: DirectiveComponent},
  {path: 'service', component: ServiceComponent},
  {path: 'form', component: FormComponent},
  {path: 'http', component: HttpComponent},
  {path: 'pipe', component: PipeComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule {
}
