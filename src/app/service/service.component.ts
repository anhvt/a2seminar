import {Component, OnInit} from '@angular/core';
import {DataService} from "./data.service";


@Component({
  selector: 'si-service',
  templateUrl: './service.component.html',
  styleUrls: ['./service.component.css'],
  providers: [DataService]
})
export class ServiceComponent implements OnInit {
  title = 'Service & Dependency Injection';

  ngOnInit() {
  }
}
