import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-directive',
  templateUrl: './directive.component.html',
  styleUrls: ['./directive.component.css']
})
export class DirectiveComponent implements OnInit {
  title = 'Directive';

  constructor() {
  }

  ngOnInit() {
  }

  private switch = true;
  private items = [1, 2, 3, 4, 5];
  private value = 10;

  onSwitch() {
    this.switch = !this.switch;
  }
}
