import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';

import {AppComponent} from './app.component';
import {MaterialModule} from "@angular/material";
import 'hammerjs';

import {AppRoutingModule} from "./app.routing";
import {DatabindingComponent} from './databinding/databinding.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {FormComponent} from './form/form.component';
import {HttpComponent} from './http/http.component';
import {DirectiveComponent} from './directive/directive.component';
import {PipeComponent} from './pipe/pipe.component';
import {PropertyBindingComponent} from "./databinding/property-binding.component";
import {EventBindingComponent} from "./databinding/event-binding.component";
import {TwoWayBindingComponent} from "./databinding/two-way-binding.component";
import {HighlightDirective} from "./directive/highlight.directive";
import {UnlessDirective} from "./directive/unless.directive";
import {DataDrivenComponent} from "./form/data-driven.component";
import {TemplateDrivenComponent} from "./form/template-driven.component";
import {DoublePipe} from "./pipe/double.pipe";
import {FilterPipe} from "./pipe/filter.pipe";
import {HttpService} from "./http/http.service";
import {ServiceComponent} from './service/service.component';
import {CmpBComponent} from "./service/cmp-b.component";
import {CmpAComponent} from "./service/cmp-a.component";
import {LogService} from "./service/log.service";

@NgModule({
  declarations: [
    AppComponent,
    DatabindingComponent,
    PropertyBindingComponent,
    EventBindingComponent,
    TwoWayBindingComponent,
    DashboardComponent,
    FormComponent,
    HttpComponent,
    DirectiveComponent,
    PipeComponent,
    PipeComponent,
    DirectiveComponent,
    FormComponent,
    HighlightDirective,
    UnlessDirective,
    DataDrivenComponent,
    TemplateDrivenComponent,
    DoublePipe,
    FilterPipe,
    ServiceComponent,
    CmpAComponent,
    CmpBComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    MaterialModule,
    AppRoutingModule,
    ReactiveFormsModule
  ],
  providers: [
    HttpService,
    LogService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
